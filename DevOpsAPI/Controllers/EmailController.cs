﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DevOpsAPI.Models;
using System.Net.Mail;
using System.Net;

namespace DevOpsAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Email")]
    public class EmailController : Controller
    {
		[HttpPost]
		public IActionResult SendMail([FromBody] Email post)
		{
			if (post == null)
			{
				return BadRequest();
			}

			SmtpClient client = new SmtpClient(post.SMTPServer);

			MailMessage mailMessage = new MailMessage
			{
				From = new MailAddress(post.From),
				To = { post.To },
				Body = post.Body,
				Subject = post.Subject
			};
			
			#region
			string username = "vhacorp\\jmorrell";
			string password = "";
#endregion

			client.UseDefaultCredentials = false;
			client.Credentials = new NetworkCredential(username, password);
			try
			{
				client.Send(mailMessage);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Exception caught in SendMail(): {0}", ex.ToString());
			}


			return Json(post);
		}
    }
}