﻿using System.Net.Mail;


namespace DevOpsAPI.Models
{
	public class Email
	{
		public string From { get; set; }
		public string To { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
		public string SMTPServer { get; set; }
	}
}
